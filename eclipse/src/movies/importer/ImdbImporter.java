/**
 * @author Christian
 * This class will give out a standardized form of the IMDB raw file
 */
package movies.importer;

import java.util.ArrayList;

public class ImdbImporter extends Processor {
	/**
	 * @author Christian
	 * 
	 * This constructor creates a ImdbImporter, creating the source and output directory
	 * @param srcDir
	 * @param outputDir
	 */
	public ImdbImporter(String srcDir, String outputDir) {
		super(srcDir, outputDir, true);
   }
	/**
	 * @author Christian
	 * 
	 * This method is going to look through the ArrayList of movies, check if the number of columns is right,
	 * and take out the name,releaseYeasr,duration and source and returns an Array of all the movies in standardized form
	 * @param input
	 * @return finalForm
	 * 
	 */
	public ArrayList<String> process(ArrayList<String> input) {
		ArrayList<String> finalForm = new ArrayList<String>();
		//Loop to iterate through the movies
		for (int i = 0; i < input.size();i++) {
			//splitting columns into an array
		   String[] moviesArray = input.get(i).split("\\t");
		   //checking if the number of columns is right
		   if(moviesArray.length == 22) {
			   //taking out needed information
		     String name = moviesArray[1];
			 String releaseYear = moviesArray[3];
			 String duration = moviesArray[6];
			 String source = "imdb";
			 //Creating an object and using toString to ensure proper form
			 Movie m = new Movie(releaseYear,name,duration,source);
			 finalForm.add(m.toString());
		   }
		   else {
			   continue;
		   }
		}
		return finalForm;
	}
}
