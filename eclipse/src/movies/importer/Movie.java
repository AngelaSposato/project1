/*
 * @author Angela
 */
package movies.importer;

public class Movie {
	private String releaseYear;
	private String name;
	private String runtime;
	private String source;
	/**
	 * @param releaseYear 
	 * @param name
	 * @param runtime
	 * @param source
	 */
	public Movie (String releaseYear, String name, String runtime, String source) {
		this.releaseYear = releaseYear;
		this.name = name;
		this.runtime = runtime;
		this.source = source;
	}
	
	/**
	 * @return releaseYear
	 */
	public String getReleaseYear() {
		return releaseYear;
	}
	
	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return runtime
	 */
	public String getRuntime() {
		return runtime;
	}
	
	/**
	 * @return source
	 */
	public String getSource() {
		return source;
	}
	
	/**
	 * Overriding toString() method
	 * @return String
	 */
	public String toString() {
		return releaseYear + "\t" +
				name + "\t" + 
				runtime + "\t" + 
				source + "\t";	
	}
	/**
	 * Overriding equals method
	 * @param o Object
	 * @return boolean true/false
	 */
	public boolean equals(Object o) {
		String runtimeNum = this.runtime;
		int runtimeInt = Integer.parseInt(runtimeNum);
		String runtimeNum2 = ((Movie) o).runtime;
		int runtimeInt2 = Integer.parseInt(runtimeNum2);
		if (this.name.equals(((Movie)o).name)  && this.releaseYear.equals(((Movie)o).releaseYear) && Math.abs(runtimeInt2 - runtimeInt) <=5) {
			return true;
		}
		else {
			return false;
		}
	}	
}
