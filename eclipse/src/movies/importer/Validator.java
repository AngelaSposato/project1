/*
 * @author Christian
 * This class will validate that the input isn't null or empty and that both the date and year
 * can be parsed into an int it will then return all the inputs that respect the rules
 */
package movies.importer;

import java.util.ArrayList;

public class Validator extends Processor {
		/**
		 * @Author Christian
		 * 
		 * Validator constructor will create an instance of the Validator class
		 * 
		 * @param srcDir
		 * @param outputDir
		 * @return newArray
		 * 
		 */
		public Validator(String srcDir, String outputDir) {
			super(srcDir, outputDir, false);
		}
		
		/**
		 * @Author Christian
		 * 
		 * @param input
		 * @return newArray
		 * 
		 */
		public ArrayList<String> process(ArrayList<String> input) {
			//Creating empty array to put all movies that respect the constraints
			ArrayList<String> newArray = new ArrayList<String>();
			//Loop to iterate through all the input movies
 			for(int i = 0;i < input.size();i++) {
 				String[] moviesArray = input.get(i).split("\\t");
 				if(moviesArray[0] != "" && moviesArray[0] != null && (!moviesArray[1].equals("")) && moviesArray[1] != null && moviesArray[2] != "" && moviesArray[2] != null && moviesArray[3] != "" && moviesArray[3] != null  ) {
 					//Try catch to try if the year and duration can be parsed into an int
 					try {
 						 Integer.parseInt(moviesArray[0]);
 						 Integer.parseInt(moviesArray[2]);
 						//If possible, it will add to the array
 						 newArray.add(input.get(i));
 					     
 					  }catch(Exception NumberFormatException) {
 						//other wise it will catch the NumberFormatException and
  						//continue without adding it to the array
 						  continue;
 					  }
 				}
 				else {
 					continue;
 				}
 			}
 			return newArray;
		}
}
