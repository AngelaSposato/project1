/**
 * @author Angela
 */
package movies.importer;

import java.util.ArrayList;


public class KaggleImporter extends Processor {
	public KaggleImporter(String srcDir, String outputDir) {
		super(srcDir, outputDir, true);
	}
	/**
	 * process is an inherited abstract method from class Processor. In this class, a new Movie is created with only certain elements from the input.
	 * The split() methodis used to split each line by tabs, and the index of the release year, title, title and source are being passed to create a new Movie object. 
	 * @param input ArrayList<String> of all movies of Kaggle file. 
	 * @return ArrayList<String> where each line represents a Movie with the release year, name, runtime, and "kaggle" as source
	 * 
	 */
	public ArrayList<String> process (ArrayList<String> input){
		ArrayList<String> finalForm = new ArrayList<String>();
		for (int i = 0; i<input.size(); i++) {
			String[] words = input.get(i).split("\\t");
			Movie addMovie = new Movie(words[20], words[15], words[13], "kaggle");
			finalForm.add(addMovie.toString());
		}	
		return finalForm;
	}
}	