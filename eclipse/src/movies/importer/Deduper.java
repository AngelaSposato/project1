/**
 * @author Christian, Angela
 */
package movies.importer;

import java.util.ArrayList;

public class Deduper extends Processor{
	public Deduper(String srcDir, String outputDir) {
		super(srcDir, outputDir, false);
	}
	
	/**
	 * This method removes all duplicates based on specific conditions and modifies the source to show that the source is from both importers
	 * @param	input	ArrayList<String> representing imdb and kaggle movies combined, which can contain duplicates of movies
	 * @return ArrayList<String> of movies with no duplicates
	 */
	public ArrayList<String> process (ArrayList<String> input){
		//contains list of all movies
		ArrayList<Movie> movieList = convertToMovie(input);
		//New one, starts out as empty
		ArrayList<Movie> noDuplicates = new ArrayList<Movie>();
		Movie currentMovie;
		//For loop going through all entries in file
		for (int i = 0; i<movieList.size(); i++) {
			//If noDuplicates does not contains, adds entry from 
			if (!noDuplicates.contains(movieList.get(i))) {
				noDuplicates.add(movieList.get(i));
			} 	
			else {
				//Takes index of where duplicate is in noDuplicates by matching same entry in movieList
				int duplicateIdx = noDuplicates.indexOf(movieList.get(i));
				String srcCurrent = movieList.get(i).getSource();
				String srcDup = noDuplicates.get(duplicateIdx).getSource();
				//If sources are not the same, add both sources and set 
				if (!srcCurrent.equals(srcDup)) {
					String appendSource = "kaggle;imdb";
					currentMovie = new Movie(movieList.get(i).getReleaseYear(), movieList.get(i).getName(), movieList.get(i).getRuntime(), appendSource);
					noDuplicates.set(duplicateIdx, currentMovie);
				} 
			}
		}
		return convertToString(noDuplicates);
	}
	
	/**
	 * Helper method that converts from ArrayList<String> to ArrayList<Movie>
	 * @param input
	 * @return convertedMovies ArrayList<Movie>
	 */
	public ArrayList<Movie> convertToMovie(ArrayList<String> input){
		ArrayList<Movie> convertedMovies = new ArrayList<Movie>();
		for (int i = 0; i<input.size(); i++) {
			String[] splitInput = input.get(i).split("\\t");
			Movie newMovie = new Movie(splitInput[0], splitInput[1], splitInput[2], splitInput[3]);
			convertedMovies.add(newMovie);
		}
		return convertedMovies;
	}
	
	/**
	 * @param input
	 * @return stringMovies ArrayList<String>
	 */
	public ArrayList<String> convertToString(ArrayList<Movie> input){
		ArrayList<String> stringMovies = new ArrayList<String>();
		for (int i = 0; i<input.size(); i++) {
			stringMovies.add(input.get(i).toString());
		}
		return stringMovies;
	}
}
