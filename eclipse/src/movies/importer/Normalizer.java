/**
 * @author Angela
 */
package movies.importer;

import java.util.ArrayList;

public class Normalizer extends Processor{
	public Normalizer(String srcDir, String outputDir) {
		super(srcDir, outputDir, false);
	}
	
	/**Input of this method is output of KaggleImporter
	 * @param input ArrayList<String> output of KaggleImport
	 * @return ArrayList<String> containing normalized movies
	 */
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> normalizedMovies = new ArrayList<String>();
		for (int i = 0; i<input.size(); i++) {
			//Splitting each entry based on tab, puts year, title, runtime, source in String[]
			String[] words = input.get(i).split("\\t");
			//Storing result of lower case name in a String
			String lowerCaseName = words[1].toLowerCase();
			//Separating runtime number and "minutes" by String
			String[] splitRuntime = words[2].split(" ");
			//Taking the first index -> number
			String runtimeNumber = splitRuntime[0];
			Movie normalizedMovie = new Movie(words[0], lowerCaseName, runtimeNumber, words[3]);
			normalizedMovies.add(normalizedMovie.toString());
		}
		return normalizedMovies;
	}
}
