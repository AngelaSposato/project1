
package movies.importer;

import java.io.IOException;

/**
 * @author Angela and Christian
 * 
 * This class will run the whole code and execute the different processors.
 */
public class ImportPipeline {

	/**
	 * @Author Angela and Christian
	 * This method creates all the source and output directories
	 * Creates the each processor Object and stores them into an array
	 * runs the processAll method with all the processor of the array
	 * @throws IOexception 
	 */
	public static void main(String[] args) throws IOException {
		//Creation of strings for the input and output directories
		String srcKaggle = "C:\\Users\\angel\\Documents\\Semester 3\\Java III\\project_1\\srcKaggle";
		String srcImdb = "C:\\Users\\angel\\Documents\\Semester 3\\Java III\\project_1\\srcImdb";
		String destinationKaggleImdb= "C:\\Users\\angel\\Documents\\Semester 3\\Java III\\project_1\\destinationKaggleImdb";
		String destinationNormalizer = "C:\\Users\\angel\\Documents\\Semester 3\\Java III\\project_1\\destinationNormalizer";
		String destinationValidator = "C:\\Users\\angel\\Documents\\Semester 3\\Java III\\project_1\\destinationValidator";
		String destinationDeduper = "C:\\Users\\angel\\Documents\\Semester 3\\Java III\\project_1\\destinationDeduper";

		//Creation of all the processors with their respective input and output directories
		KaggleImporter k = new KaggleImporter(srcKaggle, destinationKaggleImdb);
	    ImdbImporter i = new ImdbImporter(srcImdb, destinationKaggleImdb);
	    Normalizer n = new Normalizer(destinationKaggleImdb, destinationNormalizer);
	    Validator v = new Validator(destinationNormalizer, destinationValidator);
	    Deduper d = new Deduper(destinationValidator, destinationDeduper);
	    //Storing in the array
	    Processor[] processors= {k,i,n,v,d};
	    //Running processAll to start the pipeline
	    processAll(processors);
	}
	/**
	 * @Author Christian 
	 * 
	 * This method takes as input an array of processors and executes them all
	 * 
	 * @param processors
	 * @throws IOException
	 */
	private static void processAll(Processor[] processors) throws IOException {
		for(Processor p : processors) {
			p.execute();
		}
	}

}
