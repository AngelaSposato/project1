/**
 * @author Angela
 */
package movies.tests;
import movies.importer.KaggleImporter;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import java.util.ArrayList;

class KaggleImporterTests {

	
	@Test
	public void testProcess() {
		ArrayList<String> testMovie = new ArrayList<String>();
		testMovie.add("Brendan Fraser\tJohn Hannah\tMaria Bello\tMichelle Yeoh\tJet Li\tRussell Wong\t\"The Fast and the Furious director Rob Cohen continues the tale set into motion by director Stephen Sommers with this globe-trotting adventure that finds explorer Rick O'Connell and son attempting to thwart a resurrected emperor's (Jet Li) plan to enslave the entire human race. It's been 2,000 years since China's merciless Emperor Han and his formidable army were entombed in terra cotta clay by a double-dealing sorceress (Michelle Yeoh), but now, after centuries in suspended animation, an ancient curse is about to be broken. Thanks to his childhood adventures alongside father Rick (Brendan Fraser) and mother Evelyn (Maria Bello), dashing young archeologist Alex O'Connell (Luke Ford) is more than familiar with the power of the supernatural. After he is tricked into awakening the dreaded emperor from his eternal slumber, however, the frightened young adventurer is forced to seek out the wisdom of his parents -- both of whom have had their fair share of experience battling the legions of the undead. Should the fierce monarch prove capable of awakening his powerful terra cotta army, his diabolical plan for world domination will finally be set into motion. Of course, the one factor that this emperor mummy failed to consider while solidifying his power-mad plans was the O'Connells, and before this battle is over, the monstrous monarch will be forced to contend with the one family that isn't frightened by a few rickety reanimated corpses. ~ Jason Buchanan, Rovi\"\tRob Cohen\tSimon Duggan\tDirector Not Available\tAction\tPG-13\t7/24/2008\t112 minutes\tUniversal Pictures\tThe Mummy: Tomb of the Dragon Emperor\tAlfred Gough\tMiles Millar\tWriter Not Available\tWriter Not Available\t2008\t");
		KaggleImporter testKaggle = new KaggleImporter("source", "destination");
		ArrayList<String> processedMovie = testKaggle.process(testMovie);
		String[] splitted = processedMovie.get(0).split("\\t");
		String release = splitted[0];
		String name = splitted[1];
		String runtime = splitted[2];
		String source = splitted[3];
		assertEquals(1, processedMovie.size());//size
		assertEquals("2008", release);
		assertEquals("The Mummy: Tomb of the Dragon Emperor", name);
		assertEquals("112 minutes", runtime);
		assertEquals("kaggle", source); 
	}
	//Test invalid...?
}
