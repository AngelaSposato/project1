/**
 * @author Angela
 */
package movies.tests;

import movies.importer.Normalizer;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class NormalizerTests {

	@Test
	public void testNormalizer() {
		//Creating a sample ArrayList to pass to process method of Normalizer class
		ArrayList<String> testMovie = new ArrayList<String>();
		testMovie.add("2008\tthe mummy: tomb of the dragon emperor\t112\tkaggle\t");
		Normalizer testNormalizer = new Normalizer("source", "destination");
		//Storing result of process into normalizedMovie
		ArrayList<String> normalizedMovie = testNormalizer.process(testMovie);
		//Splitting ArrayList into array of Strings and storing result of each index into Strings
		String[] splitNormalizer = normalizedMovie.get(0).split("\\t");
		String release = splitNormalizer[0];
		String name = splitNormalizer[1];
		String runtime = splitNormalizer[2];
		String source = splitNormalizer[3];
		//assertEquals for unit testing
		assertEquals(1, normalizedMovie.size());
		assertEquals("2008", release);
		assertEquals("the mummy: tomb of the dragon emperor", name);
		assertEquals("112", runtime);
		assertEquals("kaggle", source);
	}

}
