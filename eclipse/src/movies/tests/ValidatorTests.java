/**
 * @author Christian
 * This class is used to test out that different problems that could occur in the Validator class
 */
package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

class ValidatorTests {
boolean x;
	@Test
	public void testValidator() {
		//In this case, the newArray should have the input string since it satisfies all the constraints;
		String input = "1894" + "\t" + "Miss Jerry" + "\t" + "45" + "\t" + "imdb" + "\t";
 			ArrayList<String> newArray = new ArrayList<String>();
 				String[] moviesArray = input.split("\\t");
 				if(moviesArray[0] != "" && moviesArray[0] != null && (!moviesArray[1].equals("")) && moviesArray[1] != null && moviesArray[2] != "" && moviesArray[2] != null && moviesArray[3] != "" && moviesArray[3] != null  ) {
 					  try {
 						 Integer.parseInt(moviesArray[0]);
 						 Integer.parseInt(moviesArray[2]);
 						newArray.add(input);
 					  }catch(Exception NumberFormatException) {
 						  //this would have a continue statement in the loop of the original class
 					  }
 				}
 				assertEquals("1894" + "\t" + "Miss Jerry" + "\t" + "45" + "\t" + "imdb" + "\t",newArray.get(0));
 				
 				//In this case, the year cannot be parsed, so it will catch the exception
 				input = "year" + "\t" + "Miss Jerry" + "\t" + "45" + "\t" + "imdb" + "\t";
 	 				moviesArray = input.split("\\t");
 	 				if(moviesArray[0] != "" && moviesArray[0] != null && (!moviesArray[1].equals("")) && moviesArray[1] != null && moviesArray[2] != "" && moviesArray[2] != null && moviesArray[3] != "" && moviesArray[3] != null  ) {
 	 					  try {
 	 						 Integer.parseInt(moviesArray[0]);
 	 						 Integer.parseInt(moviesArray[2]);
 	 						newArray.add(input);
 	 					  }catch(Exception NumberFormatException) {
 	 						  x = true;
 	 					  }
 	 				}
 	 				assertEquals(true,x);
 	 			
 	 			//In this case, the duration can't be parsed, so it will catch the exception
 	 				input = "1894" + "\t" + "Miss Jerry" + "\t" + "duration" + "\t" + "imdb" + "\t";
 	 				moviesArray = input.split("\\t");
 	 				if(moviesArray[0] != "" && moviesArray[0] != null && (!moviesArray[1].equals("")) && moviesArray[1] != null && moviesArray[2] != "" && moviesArray[2] != null && moviesArray[3] != "" && moviesArray[3] != null  ) {
 	 					  try {
 	 						 Integer.parseInt(moviesArray[0]);
 	 						 Integer.parseInt(moviesArray[2]);
 	 						newArray.add(input);
 	 					  }catch(Exception NumberFormatException) {
 	 						  x = false;
 	 					  }
 	 				}
 	 				assertEquals(false,x);
 	 				
 	 			//In this case, the name will be blank, so it will go to the else block.
 	 				input = "1894" + "\t" + "" + "\t" + "45" + "\t" + "imdb" + "\t";
 	 				moviesArray = input.split("\\t");
 	 				System.out.println(moviesArray[1].length());
 	 				if(moviesArray[0] != "" && moviesArray[0] != null && (!moviesArray[1].equals("")) && moviesArray[1] != null && moviesArray[2] != "" && moviesArray[2] != null && moviesArray[3] != "" && moviesArray[3] != null  ) {
 	 					  try {
 	 						 Integer.parseInt(moviesArray[0]);
 	 						 Integer.parseInt(moviesArray[2]);
 	 						newArray.add(input);
 	 					  }catch(Exception NumberFormatException) {
 	 						  //this would have a continue; normally
 	 					  }
 	 				}
 	 				else {
 	 					x = true;
 	 				}
 	 				assertEquals(true,x);
 			}

}
