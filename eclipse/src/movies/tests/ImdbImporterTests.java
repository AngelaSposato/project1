/**
 * @author Christian
 * This class is used to test the ImdbImporter to ensure that the information given at the end is not flawed
 */
package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.Movie;

class ImdbImporterTests {

	@Test
	public void testProcess() {
		//Creating the result it should give us(final form) and input(information we have to transform);
		ArrayList<String> finalForm = new ArrayList<String>();
		ArrayList<String> input = new ArrayList<String>();
		//adding the information
		input.add("tt0000009	Miss Jerry	Miss Jerry	1894	1894-10-09	Romance	45	USA	None	Alexander Black	Alexander Black	Alexander Black Photoplays	\"Blanche Bayliss, William Courtenay, Chauncey Depew\"	The adventures of a female reporter in the 1890s.	5.9	154					1	2");
		   String[] moviesArray = input.get(0).split("\\t");
		   if(moviesArray.length == 22) {
		     String name = moviesArray[1];
			 String releaseYear = moviesArray[3];
			 String duration = moviesArray[6];
			 String source = "imdb";
			 //Checking if all the information was well taken
			 assertEquals("Miss Jerry",name);
			 assertEquals("1894",releaseYear);
			 assertEquals("45",duration);
			 assertEquals("imdb",source);
			 
			 //Testing if the object was properly created
			 Movie m = new Movie(releaseYear,name,duration,source);
			 assertEquals("1894" + "\t" + "Miss Jerry" + "\t" + "45" + "\t" + "imdb" + "\t",m.toString());
			 
			 //Testing if the information was successfully added to the finalForm array
			 finalForm.add(m.toString());
			 assertEquals("1894" + "\t" + "Miss Jerry" + "\t" + "45" + "\t" + "imdb" + "\t",finalForm.get(0));
	}
  }
}

