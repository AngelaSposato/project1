/**
 * @author Angela 
 */
package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import movies.importer.Movie;
class MovieTests {

	@Test
	public void testMovie() {
		Movie testMovie = new Movie("2002", "My movie", "10 minutes", "Dawson College");
		assertEquals("2002", testMovie.getReleaseYear());
		assertEquals("My movie", testMovie.getName());
		assertEquals("10 minutes", testMovie.getRuntime());
		assertEquals("Dawson College", testMovie.getSource());
	}
	
	@Test 
	public void testToString() {
		Movie testMovie = new Movie("2002", "My movie", "10 minutes", "Dawson College");
		assertEquals("2002\tMy movie\t10 minutes\tDawson College\t", testMovie.toString());
	}
	@Test
	public void testEqualsValid() {
		Movie testMovie = new Movie("2002", "My movie", "10", "Dawson College");
		Movie testMovie2 = new Movie("2002", "My movie", "12", "Dawson College");
		//Expected outcome is true because titles and years match, and times are within 5 minutes
		assertEquals(true, testMovie.equals(testMovie2));
	}
	@Test 
	public void testEqualsInvalid() {
		Movie testMovie = new Movie("2002", "My movie", "10", "Dawson College");
		Movie testMovie2 = new Movie("2002", "My movie 2", "12", "Dawson College");
		//Expected outcome is false because titles of movies do not match
		assertEquals(false, testMovie.equals(testMovie2));
	}
	@Test
	public void testEqualsRuntime() {
		Movie testMovie = new Movie("2002", "My movie", "10", "Dawson College");
		Movie testMovie2 = new Movie("2002", "My movie", "17", "Dawson College");
		//Expected outcome is false because the runtimes are not within 5 minutes of each other
		assertEquals(false, testMovie.equals(testMovie2));
	}
}
